#!/usr/bin/env python
"""
AAVO briefing dashboard.
"""

import codecs
import io

from setuptools import setup, find_packages

long_description = codecs.open('README.md', "r", "utf-8").read()


def is_pkg(line):
    return line and not line.startswith(('--', 'git', '#'))

with io.open('requirements.txt', encoding='utf-8') as reqs:
    install_requires = [l for l in reqs.read().split('\n') if is_pkg(l)]

VERSION = (0, 1, 0)

setup(
    name="aavo",
    version=".".join(map(str, VERSION)),
    author='Yohan Boniface',
    author_email='yohanboniface@free.fr',
    description=__doc__,
    keywords="glider dashboard",
    url='https://framagit.org/ybon/aavo',
    packages=find_packages(),
    include_package_data=True,
    platforms=["any"],
    zip_safe=True,
    long_description=long_description,
    install_requires=install_requires,
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "Operating System :: OS Independent",
        "Topic :: Software Development :: Libraries :: Python Modules",
        "Programming Language :: Python",
    ],
 )
