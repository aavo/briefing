import io
import os
import zipfile

from flask import render_template
import requests

from .main import app, cache
from .helpers import parse_notams
from .aeroweb_proxy import get as get_aeroweb


@app.route('/')
def home():
    return render_template('home.html')


@app.route('/frame/<path:src>')
def frame(src):
    return render_template('frame.html', src=src)


@app.route('/page/<name>')
def page(name):
    return render_template('pages/{}.html'.format(name.replace('-', '_')))


@app.route('/carte/<name>', endpoint='map')
def map_(name):
    return render_template('pages/map/{}.html'.format(name))


@app.route('/pressure')
def pressure():
    params = {
        'lat': 49.0878,
        'lon': 1.6698,
        'appid': os.environ.get('OPENWEATHERMAP_KEY')
    }
    resp = requests.get('http://api.openweathermap.org/data/2.5/weather',
                        params=params)
    if not resp.status_code == 200:
        return '', resp.status_code
    return str(resp.json()['main']['pressure'])


@app.route('/notams')
@cache.cached(timeout=3600)
def notams():
    resp = requests.get(os.environ.get('NOTAMINFO_URL'))
    with zipfile.ZipFile(io.BytesIO(resp.content)) as myzip:
        with myzip.open('doc.kml') as myfile:
            return parse_notams(myfile)


@app.route('/productions-aerologiques')
@cache.cached(timeout=3600)
def bulletins():
    ids = [
        (3, "Centre"),
        (8, "Nord"),
    ]
    zones = []
    for id_, name in ids:
        content, status_code, headers = get_aeroweb('cartes_vav_log.php',
                                                    echeance='', zone=id_)
        if status_code == 200:
            zones.append((name, content.decode('latin1')))
    return render_template('pages/bulletins.html', zones=zones)
