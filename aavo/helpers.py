import json
from lxml import etree


def parse_notams(kml):
    geojson = {
        'type': 'FeatureCollection',
        'features': []
    }
    ns = "{http://www.opengis.net/kml/2.2}"
    tree = etree.parse(kml)
    for placemark in tree.findall(".//" + ns + "Placemark"):
        name = placemark.findtext(ns + "name")
        description = placemark.findtext(ns + "description")
        if 'Obstacle erected' in description:
            continue
        coords = placemark.findtext(ns + 'MultiGeometry/' + ns + "Point/"
                                    + ns + "coordinates")
        coords = list(map(float, coords.split(',')))
        geojson['features'].append({
            'type': 'Feature',
            'geometry': {
                'type': 'Point',
                'coordinates': coords
            },
            'properties': {
                'name': name,
                'description': description
            }
        })
    return json.dumps(geojson)
