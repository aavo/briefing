import datetime

from flask import render_template, redirect, request, flash
import peewee
# from wtforms import Form, SelectField, IntegerField
from wtfpeewee.orm import model_form

from .main import app, db, register_model

ZONES = (
    ('mane055', 'Mantes Est 55'),
    ('mane4500', 'Mantes Est 4500'),
    ('manw', 'Mantes Ouest'),
)


@register_model
class Settings(peewee.Model):
    pressure = peewee.IntegerField(verbose_name="Pression")
    zone = peewee.CharField(choices=ZONES, null=True, default=None)
    acrobatics = peewee.BooleanField(default=False)
    modified_at = peewee.DateTimeField(null=True)

    class Meta:
        database = db

    def save(self, *args, **kwargs):
        self.modified_at = datetime.datetime.now()
        return super().save(*args, **kwargs)

    @property
    def active_zone(self):
        return dict(ZONES).get(self.zone, '—')

    @property
    def validity_date(self):
        return self.modified_at.date()


SettingsForm = model_form(Settings)
# cf https://github.com/coleifer/wtf-peewee/commit/00207908361561c6f94b1e03b551b4d6e75fb9b2#commitcomment-22412536  # noqa
SettingsForm.zone.kwargs['validators'] = []


@app.route('/configuration', methods=['GET', 'POST'])
def settings():
    entry = get_settings()
    form = SettingsForm(request.form, obj=entry)
    if request.method == 'POST':
        if form.validate():
            form.populate_obj(entry)
            entry.save()
            flash('La configuration a été mise à jour', 'is-success')
            return redirect('/')
    return render_template('settings.html', form=form)


def get_settings():
    return Settings.select().first() or Settings()


@app.context_processor
def shared_context():
    return {
        "SETTINGS": get_settings(),
    }
