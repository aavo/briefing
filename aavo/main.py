from datetime import timezone, datetime, timedelta
import os

import click
from flask_cache import Cache
from flask import Flask
import peewee

get_env = os.environ.get

app = Flask(__name__)
app.config.update(
    SECRET_KEY=get_env('SECRET_KEY', 'sikr3t'),
)
cache = Cache(app, config={'CACHE_TYPE': 'filesystem', 'CACHE_DIR': '/tmp/'})

db = peewee.SqliteDatabase(get_env('DB_PATH', 'main.sqlite'))
MODELS = []


def register_model(model):
    MODELS.append(model)
    return model


@app.cli.command()
def initdb():
    """Initialize the database."""
    click.echo('Init the db')
    db.create_tables(MODELS)


@app.template_filter('utc2localtime')
def utc2localtime(**kwargs):
    now = datetime.now(timezone.utc).replace(**kwargs)
    local_tz = datetime.now(timezone.utc).astimezone().tzinfo
    return now.astimezone(local_tz)


@app.context_processor
def shared_context():
    return {
        "datetime": datetime,
        "timezone": timezone,
        "timedelta": timedelta,
        "utc2localtime": utc2localtime,
    }
