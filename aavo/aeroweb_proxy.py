"""
So we want to embed Aeroweb (aviation.meteo.fr) data in our dashboard. Goal
is to make the briefing time easier: less click, easier for new people to know
which pages to show, etc.
But aeroweb needs an active session to be accessed, and this session is very
short.
On a user experience perspective, the best way is to proxy everything, so this
is the path we took.
So:
- everything coming to /aeroweb/<path> will be requested to aviation.meteo.fr
- we'll manage the session, and cache the pages for fiewer calls to aeroweb
  servers
- on the fly, we'll try to patch every relative path, so they still path
  through our proxy, to get the current session

This is a lot of hacks, and there is no evidence it will work for long.
"""

from functools import wraps
from hashlib import md5
import os
import re
from urllib import parse

from flask import abort, request, Response
import requests

from .main import app

aeroweb_session = requests.Session()

PATH_PATTERN = re.compile(r'(?P<attribute>src|href|url) ?(?P<operator>=|:) ?(?P<quote>["\'])(?P<root>/)?(?P<path>(?!http|javascript)[^"\']*)["\'](?P<trail>[;+,]?)')  # noqa
CTYPE_PATTERN = re.compile(r'(?P<type>[^ ;]*)(; charset=(?P<charset>[^ ]*))?')


def aeroweb_login(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        content, status_code, headers = f(*args, **kwargs)
        # print(resp.data, resp.status_code, kwargs, request.args)
        # Some URL goes 500 when not authenticated.
        if status_code in [401, 500] or content == b'KO':
            data = {
                'login': os.environ.get('AEROWEB_LOGIN'),
                'password': md5(
                    os.environ.get('AEROWEB_PWD').encode()).hexdigest(),
            }
            # Attach session cookie.
            login = aeroweb_session.post(
                'https://aviation.meteo.fr/ajax/login_valid.php', data=data)
            if login.text == 'Login ou password invalide invalide.':
                abort('Login ou password invalide.')
            content, status_code, headers = f(*args, **kwargs)
        return content, status_code, headers
    return decorated_function


@aeroweb_login
def get(path, **params):
    resp = aeroweb_session.get('http://aviation.meteo.fr/{}'.format(path),
                               params=params)
    headers = dict(resp.headers)
    try:
        del headers['Transfer-Encoding']
    except KeyError:
        pass
    content = resp.content
    ctype = CTYPE_PATTERN.match(headers['Content-Type'])
    if ctype and ctype.group('type') in ['text/html', 'text/javascript']:

        def replace(match):
            """
            blah.php => /aeroweb/blah.php
            ./sub/blah.php => /aeroweb/sub/blah.php
            /other/blah.php => /aeroweb/other/blah.php
            """
            npath = match.group('path')
            operator = match.group('operator')
            trail = match.group('trail') or ''
            attr = match.group('attribute')
            quote = match.group('quote')
            if operator != ':' and not trail:
                # Do not escape js paths.
                npath = parse.quote(npath)
            base = '/aeroweb/'
            dirname = os.path.dirname(path)
            if not match.group('root') and dirname:
                base = base + dirname
            npath = os.path.join(base, npath)
            npath = ''.join([attr, operator, quote, npath, quote, trail])
            return npath

        content = resp.text
        content = PATH_PATTERN.sub(replace, content)
        content = content.encode(resp.encoding)
    return content, resp.status_code, headers


@app.route('/aeroweb/<path:path>', methods=['GET', 'POST'])
def aeroweb(path):
    content, status_code, headers = get(path, **dict(request.args))
    return Response(content, status_code, headers)
